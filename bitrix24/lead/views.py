from django.shortcuts import render
from .form import BitrixLeadForm

from .bitrix import create_lead


def lead(request):
    if request.method == 'POST':
        form = BitrixLeadForm(request.POST)
        if form.is_valid():
            fio = form.cleaned_data['fio']
            phone = form.cleaned_data['phone']
            address = form.cleaned_data['address']
            response = create_lead({'fio': fio, 'phone': phone, 'address': address})
            if response['status_code'] == 200:
                form = BitrixLeadForm()
                return render(request, 'lead/lead.html', {
                    'form': form,
                    'msg': 'Лид создан'
                })
            else:
                return render(request, 'lead/lead.html', {
                    'form': form,
                    'msg': f'Произошла ошибка {response["response"]}'
                })
        else:
            return render(request, 'lead/lead.html', {
                'form': form,
                'msg': 'Произошла ошибка, форма была заполнена не корректно, попробуйте ещё раз'
            })
    else:
        form = BitrixLeadForm()
        return render(request, 'lead/lead.html', {'form': form})

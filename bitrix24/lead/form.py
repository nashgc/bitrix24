from django import forms


class BitrixLeadForm(forms.Form):
    fio = forms.CharField(label='ФИО', max_length=255,  widget=forms.TextInput(attrs={
        'type': 'text',
        'aria-describedby': 'fio-label',
        'class': "form-control",
        'aria-label': 'fio',
    }))
    phone = forms.CharField(label='Телефон', max_length=100,  widget=forms.TextInput(attrs={
        'type': 'text',
        'aria-describedby': 'phone-label',
        'class': "form-control",
        'aria-label': 'phone',
    }))
    address = forms.CharField(label='Адрес', max_length=255,  widget=forms.TextInput(attrs={
        'type': 'text',
        'aria-describedby': 'address-label',
        'class': "form-control",
        'id': 'address',
        'name': 'address',
        'aria-label': 'address',
    }))

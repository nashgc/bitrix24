from urllib.parse import urljoin

import requests

from django.conf import settings


def _make_bitrix_url():
    url = urljoin(settings.BITRIX_SITE_URL, settings.BITRIX_URL_TOKEN)
    return urljoin(url, settings.BITRIX_CREATE_LEAD_ENDPOINT)


def _split_fio(fio):
    result = {
        'name': '',
        'second_name': '',
        'last_name': '',
    }
    for i, part in enumerate(fio.split(' ')):
        print(i)
        if i == 0:
            result['name'] = part
        elif i == 1:
            result['second_name'] = part
        elif i == 2:
            result['last_name'] = part
        else:
            pass
    return result


def create_lead(data):
    fio = _split_fio(data['fio'])
    print(_make_bitrix_url())
    r = requests.post(url=_make_bitrix_url(), json={
        "fields": {
            "NAME": fio['name'],
            "SECOND_NAME": fio['second_name'],
            "LAST_NAME": fio['last_name'],
            "PHONE": [{"VALUE": data['phone']}],
            "ADDRESS": data['address']
        }
    })
    return {'status_code': r.status_code, 'response': r.text}
